import HttpException from './HttpException';

class ExistingEmailException extends HttpException {
  constructor(email: string) {
    super(400, `User with ${email} already exists`);
  }
}

export default ExistingEmailException;
