import * as mongoose from 'mongoose';
import ITodo from './todos.interface';

const todoSchema = new mongoose.Schema({
  author: {
    ref: 'User',
    type: mongoose.Schema.Types.ObjectId
  },
  title: String,
  description: String,
  completed: Boolean
});

const TodoModel = mongoose.model<ITodo & mongoose.Document>('Todo', todoSchema);

export default TodoModel;
