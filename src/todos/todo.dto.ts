import { IsBoolean, IsString } from 'class-validator';

class CreateTodoDto {

  @IsString()
  public title: string;

  @IsString()
  public description: string;

  @IsBoolean()
  public completed: boolean;
}

export default CreateTodoDto;
