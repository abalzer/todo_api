interface ITodo {
  title: string;
  description: string;
  completed: boolean;
}

export default ITodo;
