import * as express from 'express';
import ITodo from './todos.interface';
import TodoModel from './todoModel';
import TodoNotFoundException from '../exceptions/TodoNotFoundException';
import HttpException from '../exceptions/HttpException';
import validationMiddleware from '../middleware/validation.middleware';
import CreateTodoDto from './todo.dto';
import authMiddleware from '../middleware/auth.middleware';
import RequestWithUser from '../interfaces/requestWithUser.interface';

class TodosController {

  public path = '/todos';
  public router = express.Router();
  private todo = TodoModel;

  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes() {
    this.router.get(this.path, this.getAllTodos);
    this.router.get(`${this.path}/:id`, this.getTodoById);
    this.router
      .all(`${this.path}/*`, authMiddleware)
      .patch(`${this.path}/:id`, validationMiddleware(CreateTodoDto, true), this.updateTodo)
      .delete(`${this.path}/:id`, this.deleteTodo)
      .post(this.path, validationMiddleware(CreateTodoDto), this.createTodo);
  }

  private getAllTodos = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    try {
      const todos = await this.todo.find().populate('author', '-password');
      response.send(todos);
    } catch (err) {
      next(new HttpException(400, `Something went wrong: ${err}`));
    }
  };

  private getTodoById = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const id = request.params.id;
    try {
      const todo = await TodoModel.findById(id);
      response.send(todo);
    } catch (err) {
      next(new TodoNotFoundException(id));
    }
  };

  private createTodo = async (request: RequestWithUser, response: express.Response, next: express.NextFunction) => {
    const todoData: CreateTodoDto = request.body;
    try {
      // TODO: failing on user being part of the request (probably just not sending postman request off correctly)
      const createdTodo = new this.todo({
        ...todoData,
        author: request.user._id
      });
      const savedTodo = await createdTodo.save();
      await savedTodo.populate('author', '-password').execPopulate();
      response.send(savedTodo);
    } catch (err) {
      next(new HttpException(400, `Something went wrong: ${err}`));
    }
  };

  private updateTodo = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const id = request.params.id;
    const todoData: ITodo = request.body;
    try {
      const updatedTodo = await this.todo.findOneAndUpdate(id, todoData, { new: true });
      response.send(updatedTodo);
    } catch (err) {
      next(new TodoNotFoundException(id));
    }
  };

  private deleteTodo = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
    const id = request.params.id;
    try {
      await this.todo.findByIdAndDelete(id);
      response.send(200);
    } catch (err) {
      next(new TodoNotFoundException(id));
    }
  };
}

export default TodosController;
