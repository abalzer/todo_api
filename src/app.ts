import * as bodyParser from 'body-parser'
import * as express from 'express';
import * as mongoose from 'mongoose';
import * as cookieParser from'cookie-parser';
import * as cors from 'cors';
import IController from './interfaces/controller.interface';
import errorMiddleware from './middleware/error.middleware';

class App {

  public app: express.Application;

  constructor(controllers: IController[]) {
    this.app = express();

    this.connectToMongoDB();
    this.initializeMiddlewares();
    this.initializeControllers(controllers);
    this.initializeErrorHandling();
  }

  public listen(): void {
    this.app.listen(process.env.PORT, () => {
      console.log(`app listening on port: ${process.env.PORT}`);
    });
  }

  private initializeMiddlewares(): void {
    this.app.use(cors());
    this.app.use(bodyParser.json());
    this.app.use(cookieParser())
  }

  private initializeControllers(controllers): void {
    controllers.forEach(controller => {
      this.app.use('/', controller.router);
    });
  }

  private initializeErrorHandling(): void {
    this.app.use(errorMiddleware);
  }

  private connectToMongoDB(): void {
    const { MONGO_USER, MONGO_PASSWORD, MONGO_PATH } = process.env;
    mongoose.connect(
      `mongodb://${MONGO_USER}:${MONGO_PASSWORD}${MONGO_PATH}`,
      { useNewUrlParser: true }
    );
  }
}

export default App;
