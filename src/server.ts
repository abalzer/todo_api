import 'dotenv/config';
import { validateEnv } from './utils/validateEnv';
import App from './app';
import TodosController from './todos/todos.controller';
import AuthenticationController from './authentication/authentication.controller';

validateEnv();

const app = new App(
  [
    new TodosController(),
    new AuthenticationController()
  ]
);

app.listen();
